# This project is intended to teach and promote Latex usage

# Gitignore submodule
It's always a pleasure to have git working nice with your files, so instead of grabbing a copy of Latex's gitignore, github's gitignore repo was added as a submodule, so you can build your own gitignores based on the languages, editors and OS that you use.


